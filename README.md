# Finmap-API

Repositório para API da aplicação Finmap, elaborada na disciplina de Algoritmos e Programação III do curso de Análise e Desenvolvimento de Sistemas da Faculdade Senac Pelotas. 

Grupo formado por:

| **Integrante** | **Email** | **GitLab** | **GitHub** |
| :------------: | :-------: | :--------: | :--------: |
| Paulo Henrique Hartwig Junior | paulohhartwig@gmail.com | [Perfil](https://gitlab.com/PauloHartwig) | [Perfil](https://github.com/PauloHartwig) |
| Vitor Hugo Sell Rodrigues | vitorsellrs@gmail.com | [Perfil](https://gitlab.com/VitorHSR)| [Perfil](https://github.com/VitorHSR) |

## Rotas

### Postman

Segue abaixo a documentação das rotas para acessar as funcionalidades básicas da API.

[Documentação das rotas da API.](https://documenter.getpostman.com/view/4145998/S1TVVcLY)

## How to Start

### Clone e Download

Clone via SSH:

`git@gitlab.com:algoritmos-iii/finmap/finmap-api.git`

Clone via HTTP:

`https://gitlab.com/algoritmos-iii/finmap/finmap-api.git`

Além disto, é possível baixar o projeto em outros formatos clicando na opção "Download".

### IDE

A aplicação foi desenvolvida utilizando a IDE Intellij. Caso queira fazer alguma mudança sem fazer alterações em outros arquivos, é recomendado que utilize a mesma IDE de desenvolvimento. 

## Heroku

A API está disponível (quando ela quer) em https://finmap-api.herokuapp.com/ + as rotas especificadas na documentação.