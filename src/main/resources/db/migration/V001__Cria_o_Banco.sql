-- --------------- TABELA USUARIO ---------------
CREATE TABLE IF NOT EXISTS usuario
(
  id        INT         NOT NULL AUTO_INCREMENT,
  nome      VARCHAR(50) NOT NULL,
  sobrenome VARCHAR(50) NOT NULL,
  email     VARCHAR(50) NOT NULL,
  senha     VARCHAR(32) NOT NULL,
  PRIMARY KEY (id)
)ENGINE = InnoDB;

-- --------------- TABELA TIPO ---------------
CREATE TABLE IF NOT EXISTS tipo
(
  id        INT         NOT NULL AUTO_INCREMENT,
  descricao VARCHAR(50) NOT NULL,
  PRIMARY KEY (id)
)ENGINE = InnoDB;

-- --------------- TABELA META ---------------
CREATE TABLE IF NOT EXISTS meta
(
  id           INT           NOT NULL AUTO_INCREMENT,
  titulo       VARCHAR(100)  NOT NULL,
  descricao    VARCHAR(500)  NULL,
  data_inicial DATE          NOT NULL,
  data_final   DATE          NOT NULL,
  valor_total  FLOAT(8, 2) NOT NULL,
  valor_atual  FLOAT(8, 2) NOT NULL,
  usuario_id   INT           NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (usuario_id) REFERENCES usuario (id)
)ENGINE = InnoDB;

-- --------------- TABELA LANCAMENTO ---------------
CREATE TABLE IF NOT EXISTS lancamento
(
  id         INT           NOT NULL AUTO_INCREMENT,
  data       DATE          NOT NULL,
  descricao  VARCHAR(100)  NOT NULL,
  valor      FLOAT(8, 2) NOT NULL,
  tipo_id    INT           NOT NULL,
  usuario_id INT           NOT NULL,
  meta_id    INT           NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (tipo_id) REFERENCES tipo (id),
  FOREIGN KEY (usuario_id) REFERENCES usuario (id),
  FOREIGN KEY (meta_id) REFERENCES meta (id)
)ENGINE = InnoDB;

-- --------------- TABELA CONTA ---------------
CREATE TABLE IF NOT EXISTS conta
(
  id           INT          NOT NULL AUTO_INCREMENT,
  descricao    VARCHAR(100) NOT NULL,
  num_parcelas INT(3)       NOT NULL,
  usuario_id   INT          NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (usuario_id) REFERENCES usuario (id)
)ENGINE = InnoDB;

-- --------------- TABELA PARCELA ---------------
CREATE TABLE IF NOT EXISTS parcela
(
  id         INT           NOT NULL AUTO_INCREMENT,
  numero     INT(3)        NOT NULL,
  vencimento DATE          NOT NULL,
  valor      FLOAT(8, 2) NOT NULL,
  status     BOOLEAN       NOT NULL,
  conta_id   INT           NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (conta_id) REFERENCES conta (id)
)ENGINE = InnoDB;

-- --------------- TABELA SALDO ---------------
CREATE TABLE IF NOT EXISTS saldo
(
  id         INT           NOT NULL AUTO_INCREMENT,
  mes        INT(2)        NOT NULL,
  ano        INT(4)        NOT NULL,
  valor      FLOAT(8, 4) NOT NULL,
  usuario_id INT           NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (usuario_id) REFERENCES usuario (id)
)ENGINE = InnoDB;