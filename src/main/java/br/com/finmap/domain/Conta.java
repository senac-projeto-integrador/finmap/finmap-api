package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.Set;

@Entity
public @Data class Conta {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "A descricao não pode ser nulo")
    @Column(nullable = false, length = 100)
    private String descricao;

    @NotNull(message = "O numParcelas não pode ser nulo")
    @Positive(message = "O numParcelas é inválido")
    @Column(nullable = false)
    private int numParcelas;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Usuario usuario;

    @JsonIgnore
    @OneToMany(mappedBy = "conta")
    private Set<Parcela> parcelas;

}
