package br.com.finmap.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public @Data class Lancamento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "A data não pode ser nulo")
    @Column(nullable = false)
    private LocalDate data;

    @NotBlank(message = "A descricao não pode ser nulo")
    @Column(nullable = false, length = 100)
    private String descricao;

    @NotNull(message = "O valor não pode ser nulo")
    @Digits(integer=8, fraction=2, message = "O valor é inválido")
    @Column(nullable = false, precision=8, scale=2)
    private float valor;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Tipo tipo;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Usuario usuario;

    @ManyToOne(cascade = CascadeType.PERSIST) // Não é obrigatório
    private @Valid Meta meta;

}
