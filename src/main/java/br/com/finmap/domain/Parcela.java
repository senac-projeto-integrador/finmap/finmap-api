package br.com.finmap.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.time.LocalDate;

@Entity
public @Data class Parcela {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "O numero não pode ser nulo")
    @Positive(message = "O numero é inválido")
    @Column(nullable = false)
    private int numero;

    @NotBlank(message = "O vencimento não pode ser nulo")
    @Column(nullable = false)
    private LocalDate vencimento;

    @NotNull(message = "O valor não pode ser nulo")
    @Digits(integer=8, fraction=2, message = "O valor é inválido")
    @Column(nullable = false, precision=8, scale=2)
    private float valor;

    @NotBlank(message = "O status não pode ser nulo")
    @Column(nullable = false)
    private Boolean status;

    @ManyToOne(cascade = CascadeType.PERSIST)
    private @Valid Conta conta;

}
