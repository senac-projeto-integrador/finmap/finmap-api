package br.com.finmap.resource;

import br.com.finmap.domain.Meta;
import br.com.finmap.repository.MetaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/meta")
public class MetaResource {

    @Autowired
    MetaRepository metaRepository;

    @GetMapping
    public List<Meta> listAll() {
        return metaRepository.findAll();
    }

    @GetMapping("/{id}")
    public Meta findOne(@PathVariable int id){
        return metaRepository.findById(id).get();
    }

    @PostMapping
    public ResponseEntity<Meta> save(@Valid @RequestBody Meta meta) {
        Meta mark = metaRepository.save(meta);
        return new ResponseEntity<Meta>(mark, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Meta> edit(@Valid @RequestBody Meta meta) {
        Meta mark = metaRepository.save(meta);
        return new ResponseEntity<Meta>(mark, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Meta> remove(@PathVariable int id) {
        Meta mark = metaRepository.findById(id).get();
        metaRepository.delete(mark);
        return new ResponseEntity<Meta>(HttpStatus.NO_CONTENT);
    }
}
